


// Smooth scroll to defined place on site
$(document).ready(function(){

	$('#contact-form input').focus(function() {
		$(this).removeClass('error');
	});
	$('#contact-form select').focus(function() {
		$(this).removeClass('error');
	});
	$('#contact-form textarea').focus(function() {
		$(this).removeClass('error');
	});	
	
	var linkHash = location.hash.substring(1);
	if (linkHash == 'contact'){
		$('#contact-us-form').modal('show')
		}
	
	//$(startTab).tab('show');

	// variable to hold request
	var request;

	// bind to the submit event of our form
	$("#contact-form").submit(function(event){
	  // abort any pending request
	  if (request) {
	      request.abort();
	  }
	  
	  // setup some local variables
	  var $form = $(this);
	  
	  // let's select and cache all the fields
	  var $inputs = $form.find("input, select, button, textarea");

	  // serialize the data in the form
	  var serializedData = $form.serialize();

	  // let's disable the inputs for the duration of the ajax request
	  $inputs.prop("disabled", true);

	  // fire off the request to /form.php
	  request = $.ajax({
	      url: "../fraudprevention/contact_form.php",
	      type: "post",
	      data: serializedData
	  });

	  // callback handler that will be called on success
	  request.done(function (response, textStatus, jqXHR){
	      // log a message to the console
	      //console.log(response);
	      var obj = jQuery.parseJSON( response );

	      if( obj.type == 'success' ){
	      	$('#contact-form').hide();
			$('.form-message').html('<p>Thanks for your submission. We\'ll be in touch.</p>').addClass('text-success').addClass('form-status');
	        
	        //console.log("form submitted");            

	      }else{
	      	//obj.returned_val
	      	//console.log(obj.returned_val);
	      	var fields = obj.returned_val.split(',');
	      	var fieldsLength = fields.length-1;

	      	$('#contact-form *').removeClass('error');

	      	for( var f=0; f<fieldsLength; f++ ){
	      		$('#contact-form *[name='+fields[f]+']').addClass('error');
	      		//console.log(fields[f]);
	      	}

	        $('.form-message').html('<p>Please correct the following fields.</p>').removeClass('text-success').addClass('text-error').addClass('form-status');
	      }
	      
	  });

	  // callback handler that will be called on failure
	  request.fail(function (jqXHR, textStatus, errorThrown){
	      // log the error to the console
	      //console.error("The following error occured: " + textStatus, errorThrown);
	  });

	  // callback handler that will be called regardless
	  // if the request failed or succeeded
	  request.always(function () {
	      // reenable the inputs
	      $inputs.prop("disabled", false);
	  });

	  // prevent default posting of form
	  event.preventDefault();
	});

	/*
		clickable area for company logo, body's padding-top and padding-bottom
		are calculated once after page loads and each time window is resized
	----------------------------------------------------------------------------- */
	var $img_header = $(".img-header");
	var $navbar_fixed_top = $(".navbar-fixed-top");
	var $navbar_fixed_bottom = $(".bottom-nav");
	var $link = $(".link")[0];
	var $body = $("body");

	function offsetImage() {
		var img_height = $img_header.height();		
		var diff = $navbar_fixed_top.height() - img_height;
		var offsety = Math.floor(diff / 2);
		$img_header.css("top",offsety);
	}

	function offsetContent() {		
		var body_padding_top = $navbar_fixed_top.height();
		body_padding_top += parseInt($navbar_fixed_top.css("margin-top"));
		body_padding_top += parseInt($navbar_fixed_top.css("margin-bottom"));
		body_padding_top += parseInt($navbar_fixed_top.css("padding-top"));
		body_padding_top += parseInt($navbar_fixed_top.css("padding-bottom"));
		$body.css("padding-top",body_padding_top);				
		
		var screen_width = $(window).width();
		if (screen_width >= 768) {
			var body_padding_bottom = $navbar_fixed_bottom.height();
			body_padding_bottom += parseInt($navbar_fixed_bottom.css("margin-top"));
			body_padding_bottom += parseInt($navbar_fixed_bottom.css("margin-bottom"));	
			body_padding_bottom += parseInt($navbar_fixed_bottom.css("padding-top"));	
			body_padding_bottom += parseInt($navbar_fixed_bottom.css("padding-bottom"));
			$body.css("padding-bottom",body_padding_bottom);
		} else {
			$body.css("padding-bottom","0px");
		}
	}

	function mouseOverLogo(e) {
		var mousex = e.pageX;
		var mousey = e.pageY;
		var img_height = $img_header.height();
		var img_width = $img_header.width();
		var offsetx = $img_header.offset().left;
		var offsety = $img_header.offset().top;

		return (				
			mousex >= offsetx &&
			mousex <= offsetx + img_width / 3.25 &&
			mousey >= offsety &&
			mousey <= offsety + img_height
		);
	}	

	/*
	$img_header.on("mousemove", function(e) {
		if (mouseOverLogo(e)) { $body.css("cursor","pointer"); }
		else { $body.css("cursor","auto"); }
	});

	$img_header.on("mouseleave", function() {
		$body.css("cursor", "auto");
	});

	$img_header.on("click", function(e) {
		if (mouseOverLogo(e)) { $link.click(); }
	});
	*/

	$(".glyphicon-upload").closest("a").on("click", function() {
		$body.scrollTop(0);
	});

	$(window).resize(function() {		
		//offsetImage();
		offsetContent();
	});
	
	//offsetImage();
	offsetContent();
	$body.scrollTop(0);	
});