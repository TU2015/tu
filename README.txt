* separated dev files from the rest of project
* placed expanded css and js files in root to make it easier other devs to find and work with
* img/header-bg.gif is not currently in use, but kept for a possible future need

Known Issues
============
* spacing around logo
* link hover color in footer "Contact Us"
* twitter social media icon blue matches footer color - possibly undesired