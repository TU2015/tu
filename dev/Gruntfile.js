module.exports = function(grunt) {  
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    
    uglify: {      
      dist: {
        src: './js/dev/custom.js',
        dest: './js/custom.min.js'
      }
    },

    cssmin: {
      combine: {
        files: {
          './css/ie8.min.css': ['./css/dev/ie8.css']
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-uglify');  
  grunt.loadNpmTasks('grunt-contrib-cssmin');  

  // Default task(s).
  grunt.registerTask('default', ['uglify','cssmin']);
};