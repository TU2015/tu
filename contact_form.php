<?php

require 'class.phpmailer.php';
require '/home/solution/creds/truerisk_credentials.php';
  
$err_message = '';

$first_name = stripcslashes($_POST['first_name']);
$last_name = stripcslashes($_POST['last_name']);
$email = stripcslashes($_POST['email']); 
$phone = stripcslashes($_POST['phone']); 
$company = stripcslashes($_POST['company']);
$state = stripcslashes($_POST['state']);
$description = stripcslashes($_POST['description']);
$sf_campaign = stripcslashes($_POST['Campaign_ID']);

function validateEmail($email) {
   if(preg_match("/^[_\.0-9a-zA-Z-]+@([0-9a-zA-Z][0-9a-zA-Z-]+\.)+[a-zA-Z]{2,6}$/i", $email)){
        return true;
   }else{
        return false;
   }
}

function validatePhone($phone) {
   if(preg_match("/^[0-9]{3}-[0-9]{3}-[0-9]{4}$/i", $phone)){
        return true;
   }else{
        return false;
   }
}

if( strlen($first_name) < 1  || $first_name == 'First Name*' ) {
    $err_message .= 'first_name,';
}
if( strlen($last_name) < 1  || $last_name == 'Last Name*' ) {
    $err_message .= 'last_name,';
}
if( strlen($email) < 1 || validateEmail($email) == FALSE ) {
    $err_message .= 'email,';
}
if( strlen($phone) < 1 || validatePhone($phone) == FALSE || $phone == '000-000-0000' ) {
    $err_message .= 'phone,';
}
if( strlen($company) < 1 || $company == 'Company*' ) {
    $err_message .= 'company,';
}
if( strlen($state) < 1 || $state == 'Select a State' ) {
    $err_message .= 'state,';
}
if( strlen($description) < 1 ) {
    $err_message .= 'description,';
}


// validation errors
if( $err_message != '' ){

  print json_encode(array('returned_val' => $err_message, 'type' => 'error'));

}else{ // all clear - success

  $successMessage = 'Thanks for registering.';

  //Initialize the $query_string variable for later use
  $query_string = "";
  
  //Initialize the $kv array for later use
  $kv = array();
   
  //For each POST variable as $name_of_input_field => $value_of_input_field
  foreach ($_POST as $key => $value) {
   
      //Set array element for each POST variable (ie. first_name=Arsham)
      $kv[] = stripslashes($key)."=".stripslashes($value);
   
  }
   
  //Create a query string with join function separted by &
  $query_string = join("&", $kv);


  //Check to see if cURL is installed ...
  if (!function_exists('curl_init')){
      die('Sorry cURL is not installed!');
  }

  //The original form action URL from Step 2 :)
  $url = 'https://www.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8';
  //$retUrl = 'http://' . $_SERVER['HTTP_HOST'] . '/index.php?screen=thanks';
   
  //Open cURL connection
  $ch = curl_init();
   
  //Set the url, number of POST vars, POST data
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_POST, count($kv));
  curl_setopt($ch, CURLOPT_POSTFIELDS, $query_string);
   
  //Set some settings that make it all work :)
  curl_setopt($ch, CURLOPT_HEADER, FALSE);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, FALSE);
  curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
   
  //Execute SalesForce web to lead PHP cURL
  $result = curl_exec($ch);
   
  //close cURL connection
  curl_close($ch);
  if($result=='ok')
  {
      $type = 'prospect';

      // log to db - prospect
      $dbh = new PDO('mysql:host=' . $host . ';dbname=' . $dbname, $user, $pass);

      $stmt = $dbh->prepare("INSERT INTO responses_fraud (first_name, last_name, email, phone, company, state, description, sf_campaign) VALUES (:first_name, :last_name, :email, :phone, :company, :state, :description, :sf_campaign)");
      $stmt->bindParam(':first_name', $first_name);
	  $stmt->bindParam(':last_name', $last_name);
	  $stmt->bindParam(':email', $email);
	  $stmt->bindParam(':phone', $phone);
	  $stmt->bindParam(':company', $company);
	  $stmt->bindParam(':state', $state);
	  $stmt->bindParam(':description', $description);
	  $stmt->bindParam(':sf_campaign', $sf_campaign);
      

      $db_email = $email;
      $db_type = $type;
      $stmt->execute();

      $dbh = null;

      print json_encode(array('returned_val' => $successMessage, 'type' => 'success', 'email' => $email));
  }else{
      $err_message = 'There was an error submitting the form. Please try again later. The administrator has been notified.';

      $mail = new PHPMailer;

      $mail->From = $email;
      $mail->FromName = $first_name;
      $mail->AddAddress('lhansen@transunion.com', 'Lauren Hansen'); // Add a recipient
      $mail->AddReplyTo($email, $first_name);

      $mail->WordWrap = 50; // Set word wrap to 50 characters
      $mail->IsHTML(true);  // Set email format to HTML

      $mail->Subject = 'Fraud Integrated Campaign: Error Submitting Form';
      $mail->Body    = 'There was an attempt to submit a form. Connection to salesforce failed. The user saw the following error: ' . $successMessage;
      $mail->AltBody = 'There was an attempt to submit a form. Connection to salesforce failed. The user saw the following error: ' . $successMessage;

      if(!$mail->Send()) {
         $err_Message = 'There was an error submitting the form and an error email could not be sent. Mailer Error: ' . $mail->ErrorInfo;
      }

      print json_encode(array('returned_val' => $err_message, 'type' => 'error'));
  }

}